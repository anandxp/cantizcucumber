package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtility {

	private static WebDriver driver = null;

	public static void initializeDriver(String string) {
		System.out.println("Selenium Utility initializeDriver");
		if (string.equalsIgnoreCase("Chrome")) {
			System.out.println("You are using Chrome Driver");
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
			driver = new ChromeDriver();
		} 
		else {
			System.out.println("Not a Chrome Driver");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		System.out.println("Escape from Driver initialize funcrion");

	}

	public static void get(String url) {
		System.out.println("Selenium Utility get");
		driver.get(url);

	}

	public static void sendKeys(String locator, String locatorvalue, String keyvalue) {
		// TODO Auto-generated method stub
		/*System.out.println("_______________Inside sendKeys() function");
		System.out.println("Locator value is : " + locatorvalue);
		System.out.println("Key value is : " + keyvalue);*/
		switch (locator) {

		case "xpath":
			driver.findElement(By.xpath(locatorvalue)).sendKeys(keyvalue);
			break;

		case "className":
			driver.findElement(By.id(locatorvalue)).sendKeys(keyvalue);
			break;

		case "cssSelector":
			driver.findElement(By.cssSelector(locatorvalue)).sendKeys(keyvalue);
			break;

		case "linkText":
			driver.findElement(By.linkText(locatorvalue)).sendKeys(keyvalue);
			break;

		case "name":
			driver.findElement(By.name(locatorvalue)).sendKeys(keyvalue);
			break;

		case "partialLinkText":
			driver.findElement(By.partialLinkText(locatorvalue)).sendKeys(keyvalue);
			break;

		case "tagName":
			driver.findElement(By.tagName(locatorvalue)).sendKeys(keyvalue);
			break;

		case "id":
			System.out.println("Inside Case ID");
			driver.findElement(By.id(locatorvalue)).sendKeys(keyvalue);
			System.out.println("Key value sent ...............");
			break;

		default:
			System.out.println("Default value inside Switch");
			break;
		}

		//System.out.println("________OUTSIDE SWITCH STATEMENT of SendKeys()_______");
	}

	public static void click(String locator, String locatorvalue) {
		// TODO Auto-generated method stub
		/*System.out.println("_______________Inside click() function");
		System.out.println("Locator is : " + locator);
		System.out.println("Locator value is : " + locatorvalue);*/
		switch (locator) {

		case "xpath":
			driver.findElement(By.xpath(locatorvalue)).click();
			break;

		case "className":
			driver.findElement(By.id(locatorvalue)).click();
			break;

		case "cssSelector":
			//System.out.println("Switched cssselector - Before click");
			driver.findElement(By.cssSelector(locatorvalue)).click();
			//System.out.println("Switched cssselector - after click");
			break;

		case "linkText":
			driver.findElement(By.linkText(locatorvalue)).click();
			break;

		case "name":
			driver.findElement(By.name(locatorvalue)).click();
			break;

		case "partialLinkText":
			driver.findElement(By.partialLinkText(locatorvalue)).click();
			break;

		case "tagName":
			driver.findElement(By.tagName(locatorvalue)).click();
			break;

		case "id":
			System.out.println("Inside Case ID");
			driver.findElement(By.id(locatorvalue)).click();
			//System.out.println("Key value sent ...............");
			break;

		default:
			System.out.println("Default value inside Switch");
			break;
		}

		//System.out.println("________OUTSIDE SWITCH STATEMENT of CLICK()_______");
	}

	public static void selectValue(String locator, String locatorvalue, String keyvalue) {
		switch (locator) {

		case "xpath":
			Select xpathselect = new Select(driver.findElement(By.xpath(locatorvalue)));
			xpathselect.selectByVisibleText(keyvalue);
			break;

		case "className":
			Select classnameselect = new Select(driver.findElement(By.className(locatorvalue)));
			classnameselect.selectByVisibleText(keyvalue);
			break;

		case "cssSelector":
			System.out.println("Switched cssselector - Before click");
			Select cssselect = new Select(driver.findElement(By.cssSelector(locatorvalue)));
			cssselect.selectByVisibleText(keyvalue);
			break;

		case "linkText":
			Select linkselect = new Select(driver.findElement(By.linkText(locatorvalue)));
			linkselect.selectByVisibleText(keyvalue);
			break;

		case "name":
			Select nameselect = new Select(driver.findElement(By.name(locatorvalue)));
			nameselect.selectByVisibleText(keyvalue);
			break;

		case "partialLinkText":
			Select partialLinkSelect = new Select(driver.findElement(By.partialLinkText(locatorvalue)));
			partialLinkSelect.selectByVisibleText(keyvalue);
			break;

		case "tagName":
			Select tagNameSelect = new Select(driver.findElement(By.tagName(locatorvalue)));
			tagNameSelect.selectByVisibleText(keyvalue);
			break;

		case "id":
			Select idSelect = new Select(driver.findElement(By.id(locatorvalue)));
			idSelect.selectByVisibleText(keyvalue);
			break;

		default:
			System.out.println("Default value inside Switch");
		}

	}

	// public static void selectValue(String elementId, String elementValue) {
	// // TODO Auto-generated method stub
	// if (elementId != null && elementValue != null) {
	// /**
	// * change to byId after getting id implemented build
	// */
	// WebElement element = driver.findElement(By.xpath(elementId));
	// Select se = new Select(element);
	// se.selectByValue(elementValue);
	//
	// }
	// }

	public static void dragAndDrop(String dragElementId, String dropElementId) {
		System.out.println("......inside Sel Utility .. Drag and Drop ... ");
		WebElement dragElement = driver.findElement(By.xpath(dragElementId));
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		// Destination of element
		//WebElement dropElement = driver.findElement(By.xpath(dropElementId));
		WebElement dropElement = driver.findElement(By.id(dropElementId));

		System.out.println("Before Action Builder");
		Actions builder = new Actions(driver);
		// group all the series of actions in Actions class
		Action dragAndDrop = builder.clickAndHold(dragElement).moveToElement(dropElement).release(dropElement).build();
		// finally we perform the actions
		System.out.println("After Action Builder");
		dragAndDrop.perform();
		System.out.println("After DragndDrop");

	}

	public static void doubleClick() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div/div/ul/li[1]/span")));
		Actions clickAction = new Actions(driver);
		clickAction.moveToElement(driver.findElement(By.xpath("/html/body/div[1]/div/div/ul/li[1]/span"))).click()
				.build().perform();
	}

}

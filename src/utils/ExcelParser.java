package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.Job;

public class ExcelParser {

	public static Job readJobDetails() throws IOException {

		Job result = new Job();
		InputStream fis = new FileInputStream(new File("C:/Selenium/Details.xlsx"));

		// Create a workbook instance that refers to excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// Create a sheet object to retrieve the sheet
		XSSFSheet sheet = wb.getSheetAt(0);

		for (Row row : sheet) {
			// Header is not considered
			if (row.getRowNum() == 1) {
				for (Cell cell : row) {
					Object cellData = getCellValue(cell);

					switch (cell.getColumnIndex()) {
					case 0:
						result.setDevice((String) cellData);
						break;

					case 1:
						result.setJobReceiver((String) cellData);
						break;

					case 2:
						result.setStatus((String) cellData);
						break;

					case 3:
						result.setClient((String) cellData);
						break;

					case 4:
						result.setEquipmentCharge((Double) cellData);
						break;

					case 5:
						result.setLabourCharge((Double) cellData);
						break;

					case 6:
						result.setOtherCharge((Double) cellData);
						break;

					case 7:
						result.setTotal((Double) cellData);
						break;

					case 8:
						result.setEta((Double) cellData);
						break;

					case 9:
						result.setCallReason((String) cellData);
						break;

					case 10:
						result.setStartDate((Double) cellData);
						break;
					case 11:
						result.setHour((Double) cellData);
						break;
					case 12:
						result.setMinute((Double) cellData);
						break;
					case 13:
						result.setAdditionalInfo((String) cellData);
						break;

					case 14:
						result.setAdditionalInfo((String) cellData);
						break;

					case 15:
						result.setContactName((String) cellData);
						break;

					case 16:
						result.setContactNumber((Double) cellData);
						break;

					case 17:
						result.setRoute((String) cellData);
						break;

					case 18:
						result.setLocation((String) cellData);
						break;

					case 19:
						result.setLocationType((String) cellData);
						break;

					case 20:
						result.setComments((String) cellData);
						break;

					case 21:
						result.setJobType((String) cellData);
						break;

					case 22:
						result.setEquipment((String) cellData);
						break;
					case 23:
						result.setAddress((String) cellData);
						break;
					case 24:
						result.setState((String) cellData);
						break;
					case 25:
						result.setCity((String) cellData);
						break;
					case 26:
						result.setCoordinates((Double) cellData);
						break;

					}
				}
			}

		}
		return result;
	}

	public static Object getCellValue(Cell cell) throws NullPointerException {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();

		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();

		case Cell.CELL_TYPE_BLANK:
			return null;
		case Cell.CELL_TYPE_FORMULA:
			return cell.getCellFormula();

		}
		return null;
	}
}

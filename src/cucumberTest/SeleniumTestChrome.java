package cucumberTest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import controller.DispatchController;
import controller.LoginController;
import controller.SubscriptionController;
import tools.DriverReader;
import tools.PropertyReader;

public class SeleniumTestChrome {

	private static WebDriver driver = null;
	static String browser = "Chrome";
	static String url = "http://52.18.84.126:4080/cantiziotclient-stg/#/";
	static Properties prop = new Properties();
	static InputStream input = null;

	public static void main(String[] args) throws InterruptedException, IOException {

		DriverReader driverReader = new DriverReader();
		driverReader.readAndConfigureDriver();

		// PropertyReader propertyReader = new PropertyReader();
		PropertyReader.setLoginCredentials();

		LoginController loginController = new LoginController();
		//DispatchController dispatchController = new DispatchController();
		SubscriptionController subscriptionController = new SubscriptionController();

		loginController.loginToCantiz(PropertyReader.accountName, PropertyReader.userName, PropertyReader.userPassword);
		//dispatchController.createNewJob();
		//dispatchController.allocateJob();
		subscriptionController.selectSubscription();

		loginController.logOutOfCantiz();

		Thread.sleep(10000);
		// driver.quit();
	}

}

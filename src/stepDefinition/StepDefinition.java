package stepDefinition;

import org.openqa.selenium.WebDriver;
//import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import controller.DispatchController;
import controller.LoginController;
import controller.SubscriptionController;
import tools.DriverReader;
import tools.PropertyReader;

public class StepDefinition {

	private static WebDriver driver = null;
	LoginController loginController = new LoginController();
	// DispatchController dispatchController = new DispatchController();
	SubscriptionController subscriptionController = new SubscriptionController();

	@Given("^User is on Login Page$")
	public void user_is_on_Login_Page() throws Throwable {
		System.out.println("User Login Page");
		DriverReader driverReader = new DriverReader();
		driverReader.readAndConfigureDriver();
		// PropertyReader propertyReader = new PropertyReader();
	}

	@When("^User enters Account UserName and Password$")
	public void user_enters_Account_UserName_and_Password() throws Throwable {
		loginController.loginToCantiz(PropertyReader.accountName, PropertyReader.userName, PropertyReader.userPassword);

	}

	@Then("^User navigated to Home Page$")
	public void user_navigated_to_Home_Page() throws Throwable {
		System.out.println("Home Page ... :)");
	}
	
	@Given("^User selects his Subscription$")
		public void user_selects_his_Subscription() throws Throwable {
		System.out.println("user_selects_his_Subscription");
		//subscriptionController.selectSubscription();
	}


	@When("^User selects Dispatch Manager$")
	public void user_selects_Dispatch_Manager() throws Throwable {
		System.out.println("user_selects_Dispatch_Manager");
		subscriptionController.selectDispatchManager();
	}


	@When("^User enters the details for the new job$")
	public void user_enters_the_details_for_the_new_job() throws Throwable {
		subscriptionController.addNewJob();
	}

	@Then("^User created a New Job$")
	public void user_created_a_New_Job() throws Throwable {
		System.out.println("Inside StepDefinition -> user_created_a_New_Job");
    	System.out.println("Subscription PropertyReader.subscription == "+PropertyReader.subscription);
		subscriptionController.setJobDetails(PropertyReader.subscription);
		subscriptionController.createNewJob();
	}
	
	@When("^User drags his job to device$")
	public void user_drags_his_job_to_device() throws Throwable {
		subscriptionController.allocateJob();

	}

	@Then("^User job is allocated$")
	public void user_job_is_allocated() throws Throwable {
		
	}
/*
	@Given("^User selects Tow Subscription$")
	public void user_selects_Tow_Subscription() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		// subscriptionController.selectSubscription();
	}

	@When("^User selects Dispatch Manager from Dispatch$")
	public void user_selects_Dispatch_Manager_from_Dispatch() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// subscriptionController.selectSideMenu();

	}

	@When("^User enters the Job Details for Tow$")
	public void user_enters_the_Job_Details_for_Tow() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// subscriptionController.addNewJob();

	}

	@Then("^User created a New Job for Tow$")
	public void user_created_a_New_Job_for_Tow() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// subscriptionController.setJobDetails();
		// subscriptionController.setJobDetailsForTow();
		// subscriptionController.createNewJob();
		// subscriptionController.addNewJob();

	}

	/*
	 * @Given("^User is on Dispatch Manager Page$") public void
	 * user_is_on_Dispatch_Manager_Page() throws Throwable {
	 * dispatchController.createNewJob();
	 * 
	 * }
	 * 
	 * @When("^User enters Device Charges Dispatch Job Details and Contact$")
	 * public void user_enters_Device_Charges_Dispatch_Job_Details_and_Contact()
	 * throws Throwable { dispatchController.setJobDetails();
	 * 
	 * }
	 * 
	 * @Then("^User created a New Job$") public void user_created_a_New_Job()
	 * throws Throwable { dispatchController.allocateJob();
	 * 
	 * }
	 */

	/*@When("^User LogOut from the Application$")
	public void user_LogOut_from_the_Application() throws Throwable {
		Assert.assertTrue(false);
	}

	@Then("^Message displayed LogOut Successfully$")
	public void message_displayed_LogOut_Successfully() throws Throwable {
	}
*/
}





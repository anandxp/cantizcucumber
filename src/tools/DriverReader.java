package tools;

import utils.SeleniumUtility;

public class DriverReader extends PropertyReader {

	public void readAndConfigureDriver() {
		  PropertyReader.setLoginCredentials();
		  PropertyReader.setSubscription();
	      SeleniumUtility.initializeDriver(browser);
	      SeleniumUtility.get(url);
	}

}

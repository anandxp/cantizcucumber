package tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

	static Properties prop = new Properties();
	static InputStream input = null;

	public static String accountName;
	public static String userName;
	public static String userPassword;
	public static String browser;
	public static String url;
	public static String subscription;

	public static void init() {
		try {

			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty("account"));
			System.out.println(prop.getProperty("user"));
			System.out.println(prop.getProperty("password"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} // end of finally

	}// end of init fn

	public static void setLoginCredentials() {
		init();
		System.out.println("Inside setLoginCredentials");
		accountName = prop.getProperty("account");
		userName = prop.getProperty("user");
		userPassword = prop.getProperty("password");

		browser = prop.getProperty("browser");
		url = prop.getProperty("url");

	}

	public static void setSubscription() {
		System.out.println("Inside PropertyReader SETSUBSCRIPTION finction");
		subscription = prop.getProperty("subscription");
		System.out.println("Sunscription is :" +subscription);

	}
}

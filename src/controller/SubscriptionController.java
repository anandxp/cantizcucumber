package controller;

import java.io.IOException;
import model.Job;
import utils.ExcelParser;
import utils.SeleniumUtility;
import tools.PropertyReader;

public class SubscriptionController {

	private Job job;

	public SubscriptionController() {
		super();
		init();
	}

	public void init() {
		try {
			job = ExcelParser.readJobDetails();
		} catch (IOException e) {
			job = null;
		}
	}

	public void selectSubscription() {
		//SeleniumUtility.selectValue("xpath", "//div[@id='welcome']/div[3]/div/div/span/span[2]", PropertyReader.subscription);
		// SeleniumUtility.selectValue("id", "tow");
		// SeleniumUtility.selectValue("id", "fibre optics");
	}

	public void selectDispatchManager() throws InterruptedException {
		// Menu Selection
		System.out.println("Before clicking Menu Button .. 5 sec sleep");
		Thread.sleep(15000);

		SeleniumUtility.click("id", "menuButton");
		System.out.println("After clicking Menu Button ... :) wait for 3 sec");
		Thread.sleep(3000);
		// Gully Dispatch Selection
		// SeleniumUtility.click("xpath",
		// "/html/body/div[1]/div/div/ul/li[1]/span");
		SeleniumUtility.doubleClick();
		// Dispatch Selection
		// SeleniumUtility.click("id", "");

		// Dispatch Manager Selection
		SeleniumUtility.click("id", "DispatchManager");

	}

	// Code to click the + Button for adding new Job
	public void addNewJob() {
		// To be implemented by Soumya Sudevan
		SeleniumUtility.click("id", "addNew");
	}

	public void setJobDetails(String subscription) throws IOException, InterruptedException {
		
		System.out.println("********** Inside setJobDetails -----");
		System.out.println("Sunscription value is :" +subscription);
		// Enter all values in drop down from the excel
		// Implement id when given by the dev team
		System.out.println("Inside set job details tab function of subscriptioncontroller");
		SeleniumUtility.selectValue("id", "deviceId", job.getDevice());
		SeleniumUtility.selectValue("id", "driverId", job.getJobReceiver());
		SeleniumUtility.selectValue("id", "statusId", job.getStatus());
		// SeleniumUtility.selectValue("id","clientId", job.getClient());
		SeleniumUtility.sendKeys("id", "storageCharge", Double.toString(job.getEquipmentCharge()));
		SeleniumUtility.sendKeys("id", "labourCharge", Double.toString(job.getLabourCharge()));
		SeleniumUtility.sendKeys("id", "otherCharges", Double.toString(job.getOtherCharge()));
		//SeleniumUtility.sendKeys("id", "total", Double.toString(job.getTotal()));
		SeleniumUtility.sendKeys("id", "eta", Double.toString(job.getEta()));
		SeleniumUtility.sendKeys("id", "callId", job.getCallReason());
		SeleniumUtility.click("id", "startDatePickerId");
		// SeleniumUtility.selectValue("id","hourSelect",
		// Double.toString(job.getHour()));
		// SeleniumUtility.selectValue("id","minuteSelect",
		// Double.toString(job.getMinute()));
		// SeleniumUtility.selectValue("id","datetimeSelect",
		// job.getMeridiem());
		
		/**** Sibil to implement id for apply button .. Proceeding with xpath for time being ***/
		//SeleniumUtility.click("id", "apply");
		
		
		/**************** END ********************/
		SeleniumUtility.click("xPath", "/html/body/div[4]/div[3]/div/button[1]");
		System.out.println("9 sec sleep after clicking Apply Button .......................");
		Thread.sleep(9000);
				
		SeleniumUtility.sendKeys("name", "Additional info", job.getAdditionalInfo());
		SeleniumUtility.sendKeys("id", "Name", job.getContactName());
		SeleniumUtility.sendKeys("id", "callbackNumber", Double.toString(job.getContactNumber()));
		SeleniumUtility.sendKeys("id", "locationType", job.getLocationType());
		SeleniumUtility.sendKeys("id", "comments", job.getComments());
		SeleniumUtility.selectValue("id", "serviceType", job.getJobType());
		SeleniumUtility.selectValue("id", "equipmentId", job.getEquipment());

		if(subscription.equalsIgnoreCase("Gully")){
			setJobDetailsForGully();
		}
		else if (subscription.equalsIgnoreCase("Tow")){
			setJobDetailsForTow();
		}
		else {
			System.out.println("Invalid Subscription");
		}
	}

	public void setJobDetailsForGully() {
		SeleniumUtility.selectValue("id", "routeId", job.getRoute());
		SeleniumUtility.selectValue("id", "locationId", job.getLocation());

	}

	public void setJobDetailsForTow() {
		SeleniumUtility.sendKeys("name", "Location Address", job.getAddress());
		SeleniumUtility.sendKeys("name", "State", job.getState());
		SeleniumUtility.sendKeys("name", "City", job.getCity());
		SeleniumUtility.sendKeys("name", "Coordinates", Double.toString(job.getCoordinates()));

	}

	public void createNewJob() {
		SeleniumUtility.click("id", "create");

	}

	public void allocateJob() throws InterruptedException {
		// SeleniumUtility.click("id", "Gully Dispatch");
		// SeleniumUtility.click("xpath",
		// "/html/body/div[1]/div/div/ul/li[1]/span"
		System.out.println("Subscri Controller --- allocateJob() function ..");
		SeleniumUtility.click("xpath",
				"/html/body/div[2]/div/div[2]/div[2]/div[1]/div/div/table/tbody/tr[4]/td[2]/button[1]");
		
		System.out.println("Inside Selenium utility - after clicking on Job left side .. Taking a 9 sec break .. ");
		
		Thread.sleep(9000);
		/*SeleniumUtility.dragAndDrop("/html/body/div[2]/div/div[2]/div/div[2]/div[1]/div[2]",
				"/html/body/div[2]/div/div[2]/div/div[2]/div[3]/table/tbody[3]/tr/td[68]/div/div");
*/
		
		SeleniumUtility.dragAndDrop("/html/body/div[2]/div/div[2]/div/div[2]/div[1]/div[2]",
				"5_16_15");

	}

}

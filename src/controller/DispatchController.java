package controller;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import model.Job;
import utils.ExcelParser;
import utils.SeleniumUtility;

public class DispatchController {

	public void createNewJob() {
		// To be implemented by Soumya Sudevan

		// Click the menu button in the home page
		// Need to change this to the corresponding ID
		SeleniumUtility.click("cssSelector", "button.menu-toggle-btn");
		// Click Dispatch Manager
		SeleniumUtility.click("linkText", "Dispatch Manager");
		// Click Add new job button
		SeleniumUtility.click("xpath", "//div[@id='content']/div/div[2]/button");

	}

	/*public void setJobDetails() throws IOException {
		Job job = ExcelParser.readJobDetails();
		// Enter all values in drop down from the excel
		//Implement id when given by the dev team
		SeleniumUtility.selectValue("id",job.getDevice());
		SeleniumUtility.selectValue("id",job.getJobReceiver());
		SeleniumUtility.selectValue("id",job.getStatus());
		SeleniumUtility.selectValue("id",job.getClient());
		SeleniumUtility.sendKeys("name", "Storage charge",Double.toString(job.getEquipmentCharge()));
		SeleniumUtility.sendKeys("name", "Labour charge",Double.toString(job.getLabourCharge()));
		SeleniumUtility.sendKeys("name", "Labour charge",Double.toString(job.getOtherCharge()));
		SeleniumUtility.sendKeys("name", "Total",Double.toString(job.getTotal()));
		SeleniumUtility.sendKeys("name", "ETA",Double.toString(job.getEta()));
		SeleniumUtility.sendKeys("name", "Call Reason", job.getCallReason());
		SeleniumUtility.click("xpath", "//div[@id='sizzle1471853445021']/div/div[2]/table/tbody/tr[4]/td[3]");
		SeleniumUtility.sendKeys("name", "Additional info", job.getAdditionalInfo());
		SeleniumUtility.sendKeys("name", "Name", job.getContactName());
		SeleniumUtility.sendKeys("name", "Callback Number", Double.toString(job.getContactNumber()));
		SeleniumUtility.selectValue("id",job.getRoute());
		SeleniumUtility.selectValue("id",job.getLocation());
		SeleniumUtility.sendKeys("name", "Location Type", job.getLocationType());
		SeleniumUtility.sendKeys("name", "Comments", job.getComments());
		SeleniumUtility.selectValue("id",job.getJobType());
		SeleniumUtility.selectValue("id",job.getJobReceiver());

	}*/

	public void allocateJob() {
		SeleniumUtility.click("id", "Gully Dispatch");
		SeleniumUtility.click("id", "Allocate Job");
		SeleniumUtility.dragAndDrop("id","id");

	}

}

package model;

import java.util.Date;

public class Job {

	String device;
	String jobReceiver;
	String status;
	String client;
	Double equipmentCharge;
	Double labourCharge;
	Double otherCharge;
	Double total;
	Double eta;
	String callReason;
	Double startDate;
	String additionalInfo;
	String contactName;
	Double contactNumber;
	String route;
	String location;
	public Double getHour() {
		return hour;
	}

	public void setHour(Double hour) {
		this.hour = hour;
	}

	public Double getMinute() {
		return minute;
	}

	public void setMinute(Double minute) {
		this.minute = minute;
	}

	public String getMeridiem() {
		return meridiem;
	}

	public void setMeridiem(String meridiem) {
		this.meridiem = meridiem;
	}

	String locationType;
	String comments;
	String jobType;
	String equipment;
	String address;
	String state;
	String city;
	Double coordinates;
	Double hour;
	Double minute;
	String meridiem;
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Double getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Double coordinates) {
		this.coordinates = coordinates;
	}

	public String getCallReason() {
		return callReason;
	}


	public Double getEquipmentCharge() {
		return equipmentCharge;
	}


	public void setEquipmentCharge(Double equipmentCharge) {
		this.equipmentCharge = equipmentCharge;
	}


	public Double getLabourCharge() {
		return labourCharge;
	}


	public void setLabourCharge(Double labourCharge) {
		this.labourCharge = labourCharge;
	}

	public Double getOtherCharge() {
		return otherCharge;
	}

	public void setOtherCharge(Double otherCharge) {
		this.otherCharge = otherCharge;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getEta() {
		return eta;
	}

	public void setEta(Double eta) {
		this.eta = eta;
	}

	public void setCallReason(String callReason) {
		this.callReason = callReason;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public Double getStartDate() {
		return startDate;
	}

	public void setStartDate(Double startDate) {
		this.startDate = startDate;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Double getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Double contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getJobReceiver() {
		return jobReceiver;
	}

	public void setJobReceiver(String jobReceiver) {
		this.jobReceiver = jobReceiver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

}

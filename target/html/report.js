$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LogIn_Test.feature");
formatter.feature({
  "line": 1,
  "name": "Login Action",
  "description": "",
  "id": "login-action",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Successful Login with Valid Credentials",
  "description": "",
  "id": "login-action;successful-login-with-valid-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "User is on Login Page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User enters Account UserName and Password",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "User navigated to Home Page",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinition.user_is_on_Login_Page()"
});
formatter.result({
  "duration": 16893583683,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_enters_Account_UserName_and_Password()"
});
formatter.result({
  "duration": 878603793,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_navigated_to_Home_Page()"
});
formatter.result({
  "duration": 139843,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Job creation based on Subscription",
  "description": "",
  "id": "login-action;job-creation-based-on-subscription",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "User selects his Subscription",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "User selects Dispatch Manager",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "User enters the details for the new job",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User created a New Job",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinition.user_selects_his_Subscription()"
});
formatter.result({
  "duration": 133382941,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_selects_Dispatch_Manager()"
});
formatter.result({
  "duration": 19400351960,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_enters_the_details_for_the_new_job()"
});
formatter.result({
  "duration": 1300513400,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_created_a_New_Job()"
});
formatter.result({
  "duration": 13568273152,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Job allocation to device",
  "description": "",
  "id": "login-action;job-allocation-to-device",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "User drags his job to device",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "User job is allocated",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinition.user_drags_his_job_to_device()"
});
formatter.result({
  "duration": 11638476523,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_job_is_allocated()"
});
formatter.result({
  "duration": 43194,
  "status": "passed"
});
});
Feature: Login Action

Scenario: Successful Login with Valid Credentials
	Given User is on Login Page
	When User enters Account UserName and Password
	Then User navigated to Home Page

Scenario: Job creation based on Subscription
    Given User selects his Subscription
    When User selects Dispatch Manager
    When User enters the details for the new job
    Then User created a New Job

Scenario: Job allocation to device
    When User drags his job to device
    Then User job is allocated